# -*- coding: utf-8 -*-
"""
Created on Sat Apr  3 18:52:52 2021

@author: user
"""

import audio_file_representation as afr
import pickle

with open('data/audio_representations.pickle', 'rb') as handle:
    loaded_structure = pickle.load(handle)

# %% assign additional semantic attributes

'''
Filename identifiers

    Modality (01 = full-AV, 02 = video-only, 03 = audio-only).

    Vocal channel (01 = speech, 02 = song).

    Emotion (01 = neutral, 02 = calm, 03 = happy, 04 = sad, 05 = angry, 06 = fearful, 07 = disgust, 08 = surprised).

    Emotional intensity (01 = normal, 02 = strong). NOTE: There is no strong intensity for the 'neutral' emotion.

    Statement (01 = "Kids are talking by the door", 02 = "Dogs are sitting by the door").

    Repetition (01 = 1st repetition, 02 = 2nd repetition).

    Actor (01 to 24. Odd numbered actors are male, even numbered actors are female).

Filename example: 03-01-06-01-02-01-12.wav

    Audio-only (03)
    Speech (01)
    Fearful (06)
    Normal intensity (01)
    Statement "dogs" (02)
    1st Repetition (01)
    12th Actor (12)
    Female, as the actor ID number is even.
'''

emotion_dict = {
    '01': 'neutral',
    '02': 'calm',
    '03': 'happy',
    '04': 'sad',
    '05': 'angry',
    '06': 'fearful',
    '07': 'disgust',
    '08': 'surprised',
}

for r in loaded_structure:
    print( 20*'-' + '\n' + r.name )
    useful_name = r.name.split('.')[0]
    splt = useful_name.split('-')
    r.actor_ID = splt[-1]
    r.female = int(r.actor_ID)%2 == 0
    r.emotion = emotion_dict[ splt[2] ]
    r.phrase_ID = splt[4]
    print( 'actor id: ' + r.actor_ID )
    print( 'female: ' + str(r.female) )
    print( 'emotion: ' + str(r.emotion) )
    print( 'phrase_ID: ' + str(r.phrase_ID) )


# %% save updated structure in pickle format
with open('data/data_representations.pickle', 'wb') as handle:
    pickle.dump(loaded_structure, handle, protocol=pickle.HIGHEST_PROTOCOL)

# %% check size of variable
print( 'size of saved structure: ' + str(len(pickle.dumps(loaded_structure, -1))) )