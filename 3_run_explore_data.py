# -*- coding: utf-8 -*-
"""
Created on Sun Apr  4 16:37:20 2021

@author: user
"""

import pandas as pd
import matplotlib.pyplot as plt
import numpy as np

# %% load prepared data
df = pd.read_pickle('data/prepared_dataframe.pickle')

# %% get a female and a male actor and compare

act01 = df[ (df['actor_ID'] == '01') & (df['phrase_ID'] == '02') ]
act02 = df[ (df['actor_ID'] == '02') & (df['phrase_ID'] == '02') ]
folder_for_figs = 'act_01_02'

# %% or another pair

act01 = df[ (df['actor_ID'] == '03') & (df['phrase_ID'] == '02') ]
act02 = df[ (df['actor_ID'] == '04') & (df['phrase_ID'] == '02') ]
folder_for_figs = 'act_03_04'

# %% male - female

# act01 = df[ (df['female'] == True) & (df['phrase_ID'] == '02') ]
# act02 = df[ (df['female'] == False) & (df['phrase_ID'] == '02') ]
# act01 = df[ df['female'] == True ]
# act02 = df[ df['female'] == False ]
act01 = df[ df['emotion'] == 'happy' ]
act02 = df[ df['emotion'] == 'sad' ]
folder_for_figs = 'happy_sad'

# %% here you can test other splits, e.g. male or female.
# You can also add information about emotion in audio_file_representation, 
# e.g., calm vs angry, and test out such categorisation tasks. For this,
# you will need to add the required information in 
# 1_run_save_data_representation.py

# %% isolate features and labels

# act01_features = np.vstack( act01['mfcc_profile'].to_numpy() )
# act02_features = np.vstack( act02['mfcc_profile'].to_numpy() )
act01_features = np.vstack( act01['features'].to_numpy() )
act02_features = np.vstack( act02['features'].to_numpy() )
all_features = np.vstack((act01_features, act02_features))

act01_labels = 0*np.ones( ( act01_features.shape[0] , 1 ) )
act02_labels = 1*np.ones( ( act02_features.shape[0] , 1 ) )
all_labels = np.r_[ act01_labels , act02_labels ]

# %% apply and plot with PCA

from sklearn.decomposition import PCA

pca = PCA(n_components=2)
pca_features = np.vstack( all_features )
all_pca = pca.fit_transform( np.vstack( pca_features ) )

plt.clf()
plt.plot( all_pca[:act01_features.shape[0], 0] , all_pca[:act01_features.shape[0], 1] , 'bx', alpha=0.8 )
plt.plot( all_pca[act01_features.shape[0]:, 0] , all_pca[act01_features.shape[0]:, 1] , 'r+', alpha=0.8 )
plt.savefig('figs/' + folder_for_figs + '/pca.png', dpi=300)

# %% apply and plot with MDS

from sklearn.manifold import MDS

mds = MDS(n_components=2)
mds_features = np.vstack( all_features )
all_mds = mds.fit_transform( np.vstack( mds_features ) )

plt.clf()
plt.plot( all_mds[:act01_features.shape[0], 0] , all_mds[:act01_features.shape[0], 1] , 'bx', alpha=0.8 )
plt.plot( all_mds[act01_features.shape[0]:, 0] , all_mds[act01_features.shape[0]:, 1] , 'r+', alpha=0.8 )
plt.savefig('figs/' + folder_for_figs + '/mds.png', dpi=300)

# %% apply and plot with TSNE

from sklearn.manifold import TSNE

tsne = TSNE(n_components=2)
tsne_features = np.vstack( all_features )
all_tsne = tsne.fit_transform( np.vstack( tsne_features ) )

plt.clf()
plt.plot( all_tsne[:act01_features.shape[0], 0] , all_tsne[:act01_features.shape[0], 1] , 'bx', alpha=0.8 )
plt.plot( all_tsne[act01_features.shape[0]:, 0] , all_tsne[act01_features.shape[0]:, 1] , 'r+', alpha=0.8 )
plt.savefig('figs/' + folder_for_figs + '/tsne.png', dpi=300)
