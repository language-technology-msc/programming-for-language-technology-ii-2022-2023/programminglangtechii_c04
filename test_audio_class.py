# -*- coding: utf-8 -*-
"""
Created on Sat Apr  3 18:23:43 2021

@author: user
"""

import sys
import audio_file_representation as afr
import pickle

file_path = 'test_files/03-01-01-01-01-01-02.wav'
# file_path = 'test_files/03-01-01-01-01-01-01.wav'

# %% load object

test_obj = afr.AudioFileRepresentation( file_path , keep_audio=True , keep_aux=True )

# %% test analysis-related constanstants - n_fft, hop_size

# %% check size
print( 'size of object: ' + str(sys.getsizeof(test_obj)) )
print( 'more accurate size of object: ' + str(len(pickle.dumps(test_obj, -1))) )